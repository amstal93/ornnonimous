from cell.cell import Cell
import netstruct

CREATE_CMD = 1
HTYPES = {'GET_PUB': 1, 'SHARED_SECRET': 2}
field_names = ['circID', 'cmd', 'hType', 'hLen', 'hData']

class Create(Cell):
    def __init__(self, circ_id, htype, hlen, hdata):
        super().__init__(circ_id, CREATE_CMD)
        self._htype = htype
        self._hlen = hlen
        self._hdata = hdata

    # overriding abstract method
    def create_msg(self) -> bytearray:
        return netstruct.pack(b'i B h h', self._circ_id, self._cmd, self._htype, self._hlen) + self._hdata
    
    @staticmethod
    def unpack_msg(msg):
        unpacked = netstruct.unpack(b"i B h h", msg)
        dict = {k: v for k, v in zip(field_names, unpacked)}
        dict['hData'] = msg[9:]
        return dict


        

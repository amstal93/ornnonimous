import sys
import os
current_dir = os.path.dirname(os.path.realpath(__file__))
parent = os.path.dirname(current_dir)
sys.path.append(parent)

import json
from Cryptodome.PublicKey import RSA as RSACipher
import RSA
from AES import Aes
import socket
import base64
import random

RTYPE = {'GET_PUB': '1', 'GET_PRV': '2', 'GET_NODES':'3'}

def sendData(conn, data):
    conn.sendall(data.encode())
   
def getData(conn):
    ans = conn.recv(1024).decode()
    return load_directory_msgs(ans)

def create_msg(request, data):
    answer = {'request': request, 'data': data}
    return json.dumps(answer)

def load_directory_msgs(msg):
    return json.loads(msg)

def key_exchange(conn, aesKey):
    # sending pub key request 
    sendData(conn,create_msg(RTYPE['GET_PUB'], ' '))
    # geting pub key 
    ans = getData(conn)
    if ans['isValide'] == '0':
        return 0

    # sending aes key 
    rsaKey = RSACipher.import_key(ans['ans'].encode())
    encAesKey = base64.b64encode(RSA.encrypt(aesKey.get_key(), rsaKey)).decode()
    sendData(conn,create_msg(RTYPE['GET_PRV'], encAesKey))

    #get confirmation msg
    ans = getData(conn)
    if ans['isValide'] == '0':
        return 0
    else:
        return 1


def ask_for_nodes(conn, aesKey, numOfNode):
    """
    creat request for nodes send directory nodes the request 
    return the list of nodes
    the reqest is encrypted be aesKey- the aes key 
    """
    request = base64.b64encode(aesKey.encrypt(RTYPE['GET_NODES'].encode())).decode()
    data = base64.b64encode(aesKey.encrypt(str(numOfNode).encode())).decode()
    sendData(conn,create_msg(request, data))
    ans = getData(conn)
    ans['isValide'] = aesKey.decrypt(base64.b64decode(ans['isValide'].encode())).decode()
    ans['ans'] = aesKey.decrypt(base64.b64decode(( ans['ans'].encode()))).decode()
    if ans['isValide'] == '0':
        return 0
    return json.loads(ans['ans'])


def get_nodes(ip, port, num_of_nodes):
    """
    returning nodes from directory node:
    returns a list of dictionaries, each containing the node address, AES key, and a circuit-ID.
    """
    aes_key = Aes()
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((ip, port))
        try:
            key_exchange(s, aes_key)
            nodes = ask_for_nodes(s, aes_key, num_of_nodes)
        except Exception as e:
            print(e)
            return []

        relay_list=[]
        for node in nodes:
            relay_list += [{'connection': (node[2], node[1]), 'Aes_key': Aes(), 'circ_id':  random.randint(1, 50)}]
    return relay_list


from flask import Blueprint, render_template, url_for, request, redirect, flash
from flask_login import login_user, logout_user, login_required, current_user
from .models import Message, User, Chat
from . import db, user_list
import hashlib


auth = Blueprint('auth', __name__)
num_anonymous = 5

@auth.route('/login')
def login():
    return render_template('login.html')

@auth.route('/login', methods=['POST'])
def login_post():
    email = request.form.get('email')
    password = request.form.get('password')

    user = User.query.filter_by(email=email).first()
    # checking if user exists and comparing the password hashes
    if not user or hashlib.sha256(password.encode()).hexdigest() != user.password or len(password) == 0:
        flash('Please check your login details and try again.')
        return redirect(url_for('auth.login'))
    
    login_user(user, remember=True)
    user_list['users'] += [{'id': user.id, 'name': user.name}]
    return redirect(url_for('main.index'))

@auth.route('/login_anonymous', methods=['POST'])
def login_anonymous():
    global num_anonymous
    name = 'anonymous' + str(num_anonymous)  # a name for the anonymous user
    email = name + '@gmail.com'
    user = User(email=email, name=name, password='')  # password is empty
    db.session.add(user)
    db.session.commit()
    num_anonymous += 1
    
    login_user(user, remember=True)
    user_list['users'] += [{'id': user.id, 'name': user.name}]
    return redirect(url_for('main.index'))

@auth.route('/signup')
def signup():
    return render_template('signup.html')

@auth.route('/signup', methods=['POST'])
def singup_post():
    email = request.form.get('email')
    name = request.form.get('name')
    password = request.form.get('password')

    if len(name) == 0 or len(password) == 0 or len(email) == 0:
        flash('Please check your login details and try again.')
        return redirect(url_for('auth.signup'))

    # when user already exists
    user = User.query.filter_by(email=email).first()
    if user:
        flash('Email address already exists.')
        return redirect(url_for('auth.signup'))

    new_user = User(email=email, name=name, password=hashlib.sha256(password.encode()).hexdigest())  # hashing the password

    # add new user to db
    db.session.add(new_user)
    db.session.commit()

    return redirect(url_for('auth.login')) 

    
@auth.route('/logout')
@login_required
def logout():
    for user in user_list['users']:
        if user['id'] == int(current_user.get_id()):
            user_list['users'].remove(user)
            
    user = User.query.filter_by(id = int(current_user.get_id())).first()

    if len(user.password) == 0:  # deleting the anonymous user and all of his chats and messages
        User.query.filter_by(id=user.id).delete() # deleting user
        chats_to_delete=[]

        for chat in Chat.query.filter_by().all():
            if chat.A_userId == user.id or chat.B_userId == user.id:
                chats_to_delete += [chat.id]
        print('chat IDs to delete: ', chats_to_delete)
        # deleting the user's messages
        for chatId in chats_to_delete:
            all_msg = Message.query.filter_by(chatId=chatId).all()
            print(all_msg)
            for msg in all_msg:
                db.session.delete(msg)
        # deleting user's chats
        for chatId in chats_to_delete:
            Chat.query.filter_by(id=chatId).delete()
        db.session.commit()

    logout_user()
    return redirect(url_for('auth.login'))

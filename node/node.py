import _thread
import socket
import sys
import os

#sys.path.append('/home/kali/ornnonimous')
current_dir = os.path.dirname(os.path.realpath(__file__))
parent = os.path.dirname(current_dir)

sys.path.append(parent)

from RSA import Rsa
from AES import Aes
from cell.create import Create, CREATE_CMD, HTYPES
from cell.created import Created, CREATED_CMD
from cell.relay import RCMDS, Relay, RELAY_CMD
from cell.destroy import Destroy, DESTROY_CMD, DESTROY_CODES
from decapsulate import decapsulate
from encapsulate import encapsulate

LISTEN_HOST = os.environ['LISTEN_HOST']
LISTEN_PORT = int(os.environ['LISTEN_PORT'])
TIMEOUT = 45
BUFFER_SIZE = 100000

rsa = Rsa()

class Node():
    def __init__(self) -> None:
        self._stream = {'prev_hop':{'circID': None, 'sock': None}, 'next_hop':{'circID': None, 'sock': None}}
        self._key = 0
        self._is_alive = True
        
    def get_is_alive(self):
        return self._is_alive
        
    def tear_circuit(self):
        """
        closing connections and Tearing circuit down
        """
        if self._stream['prev_hop']['sock'] == None:
            return
        print('Tearing circuit down')
        # creating Destroy msgs for both connection
        destroy_prev = Destroy(self._stream['prev_hop']['circID'], DESTROY_CODES['CHANNEL_CLOSED']).create_msg()
        if self._stream['next_hop']['circID'] != 'server':  # dont send server Destroy msg
            destroy_next = Destroy(self._stream['next_hop']['circID'], DESTROY_CODES['CHANNEL_CLOSED']).create_msg()
        
        try:
            # sending Destroy msgs for all
            self._stream['prev_hop']['sock'].sendall(destroy_prev)  # send destroy to prev hop
            if self._stream['next_hop']['circID'] != 'server' and self._stream['next_hop']['circID'] != None:  # send the msg only if it's valid to send the msg 
                self._stream['next_hop']['sock'].sendall(destroy_next)  # sent destroy to next hop
        except Exception as e:
            pass  # if got here next or previous node is down 
        finally:
            self._stream['prev_hop']['sock'].close()
            if self._stream['next_hop']['circID'] != None:
                self._stream['next_hop']['sock'].close()
            self._is_alive = False
            sys.exit()


    def send_and_recv(self, msg):
        """
        Sending and receiving response from next hop
        """
        try:
            self._stream['next_hop']['sock'].sendall(msg)  # send message
            recv = b''
            if self._stream['next_hop']['circID'] != 'server':  # if next hop is a node 
                recv = self._stream['next_hop']['sock'].recv(BUFFER_SIZE)
                print('recieved from node: ', recv)
            else:  # if next hop is the server
                result = self._stream['next_hop']['sock'].recv(BUFFER_SIZE)
                while (len(result) > 0): # keep receiving from server
                    recv += result
                    result = self._stream['next_hop']['sock'].recv(BUFFER_SIZE)
                print('recievd from server: ', recv)
            if not recv:
                # if received empty msg from next hop, tearing circuit
                self.tear_circuit()
            return recv
        except:  # if cant recive or send message 
            self.tear_circuit()
            

    def handle_destroy_request(self, req):
        self.tear_circuit()

    
    def handle_create_request(self, req, conn):
        """
        Handles the create requests: 
        sharing node's public key with client, getting client's AES key
        returns response for the request
        """
        req = Create.unpack_msg(req)

        if req['hType'] == HTYPES['GET_PUB']:
            # returning public key
            print('Get public accepted: ', req)
            hdata = rsa.get_public_key()
            created = Created(req['circID'], len(hdata), hdata).create_msg()
            return created
        
        elif req['hType'] == HTYPES['SHARED_SECRET']:
            # decrypting client's AES key with out private RSA key:
            print('Establish secret accepted: ', req)
            dec = rsa.decrypt(req['hData'])
            self._key = Aes(dec)

            # save information about previous node 
            self._stream['prev_hop']['circID'] = req['circID']
            self._stream['prev_hop']['sock'] = conn

            hdata = b' Success'
            created = Created(req['circID'], len(hdata), hdata).create_msg()
            return created
        else:
            return b''

    def handle_relay_request(self, req):
        """
        Handling all kinds of relay requests (DATA, BEGIN, EXTEND)
        returns encrypted response for the request
        """
        print ("res msg - ", req)
        req = decapsulate(req, self._key)
        if req['rcmd'] == RCMDS['DATA']:
            return self._relay_data(req)
        elif req['rcmd'] == RCMDS['BEGIN']:
            return self._relay_begin(req)
        elif req['rcmd'] == RCMDS['EXTEND']:
            return self._relay_extend(req)
        else:
            return -1

    def _relay_begin(self, req):
        """
        Connects the exit node to the server
        returns encrypted relay CONNECTED response     
        """
        server_ip, sererv_port = req['payload'][:4], int.from_bytes(req['payload'][4:6], "big")
        server_string_ip = get_ip_from_bytes(server_ip)
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            s.connect((server_string_ip, sererv_port))
            s.settimeout(TIMEOUT)
        except Exception:
            print('could not connect to ', (server_string_ip, sererv_port))
        
        self._stream['next_hop']['circID'] = 'server'  # this indicates that the next hop is a server, this is the exit node
        self._stream['next_hop']['sock'] = s  
        
        relay = Relay(circ_id=req['circID'], rcmd=RCMDS['CONNECTED'], streamID=req['streamID'], recognized=req['recognized'], digest=req['digest'], length=len(server_ip), data=server_ip)
        res = encapsulate([[self._key,relay]])
        return res
        

    def _relay_data(self, req):
        """
        Send message to next hop
        returns encrypted answer to previous hop
        """
        print('Relay accepted: ', req)
        res = self.send_and_recv(req['payload'])  # send message
        relay = Relay(circ_id=req['circID'], rcmd=req['rcmd'], streamID=req['streamID'], recognized=req['recognized'], digest=req['digest'], length=len(res), data=res)  # create cell
        res = encapsulate([[self._key,relay]])
        return res
    
    def _relay_extend(self, req):
        """"
        Handle the relay extend request: adding another node to the circuit
        returns encrypted relay EXTENDED response  
        """
        print('Extend accepted: ', req)
        next_hop_ip, next_hop_port = req['payload'][:4], int.from_bytes(req['payload'][4:6], "big")
        next_hop_ip = get_ip_from_bytes(next_hop_ip)
        print("connecting to: ", (next_hop_ip, next_hop_port))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((next_hop_ip, next_hop_port))
        s.settimeout(TIMEOUT)
        next_relay_info = Create.unpack_msg(req['payload'][6:])
        self._stream['next_hop']['circID'] = next_relay_info['circID']
        self._stream['next_hop']['sock'] = s

        created_msg = self.send_and_recv( req['payload'][6:]) # send create msg to next hop

        relay = Relay(circ_id=req['circID'], rcmd=RCMDS['EXTENDED'], streamID=req['streamID'], recognized=req['recognized'], digest=req['digest'], length=len(created_msg), data=created_msg)
        res = encapsulate([[self._key,relay]])
        print("Encapsulating: ", res)

        return res

    

def handle_conn(conn, addr):
    """
    handle connection, direct msg to right handlers
    """
    node = Node()
    while node.get_is_alive():
        try:
            req = conn.recv(BUFFER_SIZE)
        except:
            node.tear_circuit()

        if len(req) < 5: # message size cant be less then 5 (the headers) or message is empty 
            print('connection with ', addr, ' closed')
            node._is_alive = False
            node.tear_circuit()
            break
        
        # Handling request by its command type
        cmd = req[4]
        if cmd == CREATE_CMD:
            res = node.handle_create_request(req, conn)
        elif cmd == RELAY_CMD:
            res = node.handle_relay_request(req)
        elif cmd == DESTROY_CMD:
            node.handle_destroy_request(req)
        else:
            print(req)
            res = b'ERROR'
        
        try:
            conn.sendall(res)
        except:
            node.tear_circuit()
    conn.close()
    


def get_ip_from_bytes(bIp):
    """
    converts bytes to string, in ip format , returns ip string 
    """
    return '.'.join(str(b) for b in bIp)
    



def main():
    print("node - ", LISTEN_HOST,"on port ",LISTEN_PORT)
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        print('binding')
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        s.bind((LISTEN_HOST, LISTEN_PORT))

        s.listen()
        print('listening')

        while True:
            conn, addr = s.accept()
            conn.settimeout(TIMEOUT) # this makes the node stop recv function it too much time have passed
            print('connected by ', addr)
            _thread.start_new_thread(handle_conn, (conn, addr,))

if __name__ == '__main__':
    main()
import sqlite3

class NodesRecords():
    def __init__(self, db_file) -> None:
        self.conn = None
        try:
            self.conn = sqlite3.connect(db_file)
        except sqlite3.Error as e:
            raise e
        self.cur = self.conn.cursor()
        self.init_table()

    def __del__(self):
        self.conn.close()

    def init_table(self):
        self.cur.execute('CREATE TABLE IF NOT EXISTS Nodes( \
        id INTEGER PRIMARY KEY AUTOINCREMENT, \
        port INTEGER not null, \
        ip TEXT not null, \
        status BOOLEAN DEFAULT FALSE not null \
        );')
    
    def addRecords(self, port, ip):
        self.cur.execute("insert into Nodes (ip, port) VALUES('" + ip + "', " + str(port) + ");")
        self.conn.commit()
    
    
    def updateStatus(self, nodeIp, status):
        self.cur.execute("UPDATE Nodes SET status = " + str(status)+ " WHERE ip = '" + nodeIp + "';")
        self.conn.commit()


    def deleteRecords(self):
        self.cur.execute("DELETE FROM Nodes;")
        self.conn.commit()


    def get_nodes(self, is_alive=True):
        query = ''
        if is_alive:
            query = 'SELECT * FROM nodes where status = 1;'
        else:
            query = 'SELECT * FROM nodes;'
        ans = self.cur.execute(query)
        return [row for row in ans]
    

def main():
    db = NodesRecords("nodesDB.sqlite")
    #db.deleteRecords()
    db.addRecords(5555, '192.168.55.13')
    db.addRecords(5555, '192.168.55.14')
    print(db.get_nodes(False))
if __name__ == '__main__':
    main()

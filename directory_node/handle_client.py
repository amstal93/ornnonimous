import sys
import os
current_dir = os.path.dirname(os.path.realpath(__file__))
parent = os.path.dirname(current_dir)
sys.path.append(parent)

import json
from RSA import Rsa
from AES import Aes
import base64
from nodes_records import NodesRecords
import random



RTYPE = {'GET_PUB': '1', 'GET_PRV': '2', 'GET_NODES':'3'}

def sendData(conn, data):
    # send data
    try:
        conn.sendall(data.encode())
    except Exception  as e:
        raise e

def getData(conn):
    # get answer from connection and returns it as json
    try:
        ans = conn.recv(1024).decode()
        return lode_client_mgs(ans)
    except Exception  as e:
        raise e

def creat_msg(isValide, ans):
    answer = {'isValide': isValide, 'ans': ans}
    answer = json.dumps(answer)
    return answer

def lode_client_mgs(msg):
    return json.loads(msg)

def send_error_msg(conn, errorMsg):
    # create and send an error message 
    sendData(conn, creat_msg('0',errorMsg))

def handle_client(conn, addr,rsaKay,db):
    try:
        aesKey = key_exchang(conn,rsaKay)
        print (aesKey.get_key())
        give_nodes(conn,db,aesKey)
    except Exception as e:
        raise e
    finally:
        conn.close()



def key_exchang(conn, rsaKay):
    """
    the function does the key exchang 
    """
    #get pub key reqest 
    ans = getData(conn)
    if ans['request'] != RTYPE['GET_PUB']:
        send_error_msg(conn,'got an unexpected msg')

    #send pubKey RSA key
    pubKey = rsaKay.get_public_key().decode()
    sendData(conn,creat_msg('1', pubKey))
    
    #gat privet aes key
    ans = getData(conn)
    if ans['request'] != RTYPE['GET_PRV']:
        send_error_msg(conn,'got an unexpected msg')
    ans['data']
    aesKey = Aes(rsaKay.decrypt(base64.b64decode(ans['data'].encode())))

    #sand confirmation msg
    sendData(conn,creat_msg('1', 'got aes key'))
    return aesKey

def give_nodes(conn, db, aesKey):
    """
    get nodes from database, fix the information to the right structure 
    and send them to user 
    """
    #get nodes reqest 
    ans = getData(conn)
    ans['request'] = aesKey.decrypt(base64.b64decode(ans['request'].encode())).decode()
    ans['data'] = int(aesKey.decrypt(base64.b64decode(ans['data'].encode())))

    if ans['request'] != RTYPE['GET_NODES']:
        send_error_msg(conn,'got an unexpected msg')

    allNodes = NodesRecords(db).get_nodes()
    print("nodes that dir give - ", allNodes)
    nodes = []
    if ans['data'] <= len(allNodes) and ans['data'] > 0:
        nodes = random.sample(allNodes, ans['data'])
    print("nodes that dir give - ", nodes)
    nodes = base64.b64encode(aesKey.encrypt(json.dumps(nodes).encode())).decode()
    isValide = base64.b64encode(aesKey.encrypt(b'1')).decode()
    sendData(conn,creat_msg(isValide, nodes))





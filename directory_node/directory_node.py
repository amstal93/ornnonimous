import sys
import os
current_dir = os.path.dirname(os.path.realpath(__file__))
parent = os.path.dirname(current_dir)
sys.path.append(parent)

import _thread
import socket
from RSA import Rsa
from AES import Aes
from nodes_records import NodesRecords
from handle_client import handle_client
from node_status import updateNodeStatus


LISTEN_HOST = '0.0.0.0'
LISTEN_PORT = 1222 

PUB_KEY = Rsa()
DB = "./directory_node/nodesDB.sqlite"

def main():
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.bind((LISTEN_HOST, LISTEN_PORT)), print("bind ")
        s.listen(), print("listen ")
        # Thread updates the state of all nodes in db
        _thread.start_new_thread(updateNodeStatus, (DB,))

        # Client connection
        while True:
            conn, addr = s.accept()
            print ("new client ", addr)
            _thread.start_new_thread(handle_client, (conn, addr, PUB_KEY, DB))

if __name__ == '__main__':
    main()

    
    
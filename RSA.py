from Cryptodome.PublicKey import RSA
from Cryptodome.Cipher import PKCS1_OAEP

class Rsa:
    def __init__(self, bits=1024):
        '''
        Generate an RSA keypair with an exponent of 65537 in PEM format
        param: bits The key length in bits
        Return private key and public key
        '''
        self.private_key = RSA.generate(bits, e=65537) 
        self.public_key = self.private_key.publickey()

    def encrypt(self, plaintext):
        cipher = PKCS1_OAEP.new(self.public_key)
        return cipher.encrypt(plaintext)

    def decrypt(self, enc):
        cipher = PKCS1_OAEP.new(self.private_key)
        return cipher.decrypt(enc)

    def get_public_key(self):
        return self.public_key.export_key()

def encrypt(plaintext, public_key):
        cipher = PKCS1_OAEP.new(public_key)
        return cipher.encrypt(plaintext)


from  cell.create import Create
from  cell.created import Created
from  cell.relay import RCMDS, Relay
from AES import Aes
import netstruct

RELAY_CMD = 3
CREATED_CMD = 2
CREATE_CMD = 1

def decrypt_msg(aes, msg):
    headers = msg[:5]
    payload = bytes(aes.decrypt(msg[5:]))
    return headers+payload

def get_cell_headers(msg):
    headers = netstruct.unpack(b"i B", msg)
    return headers[1]


def decapsulate(msg ,aes = None):
    """
    Decapsulate a single layer from the tor cell
    Params: msg - tor cell, aes - symetric key used to decrypt current layer
    Return: the peeled cell
    """
    command = get_cell_headers(msg)
    if aes != None:
        msg = decrypt_msg(aes, msg)
    if command == RELAY_CMD:
         return Relay.unpack_msg(msg)
    elif command == CREATED_CMD: 
        return Created.unpack_msg(msg)
    elif command == CREATE_CMD:  # if got this msg need to return to client 
        return Create.unpack_msg(msg)
    else:
        print('no command')
        return -1


def decapsulate_all(msg, key_list):
    """
    Decapsulate a full tor cell
    Params: msg - tor cell, key_list - symetric keys in the order of decription
    Return: the final response
    """
    if len(key_list)==0:
        return msg
    else:
        for aes in key_list:
            decrypted_msg = decrypt_msg(aes,msg)
            cell = decapsulate(decrypted_msg)
            msg = cell['payload']
        return msg



